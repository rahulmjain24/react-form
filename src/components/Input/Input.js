import React from 'react'

class Input extends React.Component {
    render() {
        return (
            <div className="mb-3">
                <label htmlFor={this.props.for} className="form-label">{this.props.lable}</label>
                <div className="mb-1 input-group">
                    <span className="input-group-text">
                        <img 
                            src={this.props.icon} 
                            alt="icon" 
                        />
                    </span>
                    <input
                        onChange={(e) => {
                            this.props.submit(e)
                        }}
                        type={this.props.type}
                        className={`form-control ${this.props.validation}`}
                        id={this.props.for}
                        placeholder={this.props.placeholder}
                        required
                    />
                    <div className="invalid-feedback">
                        {this.props.message}
                    </div>
                </div>

            </div>
        )
    }
}

export default Input