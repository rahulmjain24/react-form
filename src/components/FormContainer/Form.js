import React from 'react'
import "./Form.css"
import Input from '../Input/Input'
import Select from '../Select/Select'
import Btn from '../Btn/Btn'
import Check from '../Check/Check'
import validator from 'validator'

class Form extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            firstName: { value: "", isValid: false, isModified:false },
            lastName: { value: "", isValid: false, isModified:false },
            age: { value: 0, isValid: false, isModified:false },
            gender: {value: "", isValid:false, isModified:false },
            role: {value:"", isValid:false, isModified:false },
            email: { value: "", isValid: false, isModified:false },
            password: { value: "", isValid: false, isModified:false },
            conPassword: { value: "", isValid: false, isModified:false },
            checked: {isValid: false, isModified : false},
            isValidForm: false,
        }
    }

    isValidAge = (e) => {
        if (parseInt(e.target.value) > 0 && parseInt(e.target.value) < 150 && !e.target.value.includes('.')) {
            this.setState({ age: { value: parseInt(e.target.value.trim()), isValid: true, isModified : true} })
        } else {
            this.setState({ age: { value: parseInt(e.target.value.trim().trim()), isValid: false, isModified : true} })
        }
    }

    isValidName = (e) => {
        if (e.target.id === 'first') {
            if (validator.isEmpty(e.target.value.trim(), { ignore_whitespace: true }) || !validator.isAlpha(e.target.value.trim())) {
                this.setState({ firstName: { value: e.target.value.trim(), isValid: false, isModified : true} })
            } else {
                this.setState({ firstName: { value: e.target.value.trim(), isValid: true, isModified : true } })
            }
        }
        if (e.target.id === 'last') {
            if (validator.isEmpty(e.target.value.trim(), { ignore_whitespace: true }) || !validator.isAlpha(e.target.value.trim())) {
                this.setState({ lastName: { value: e.target.value.trim(), isValid: false, isModified : true} })
            } else {
                this.setState({ lastName: { value: e.target.value.trim(), isValid: true, isModified : true} })
            }
        }
    }

    isValidEmail = (e) => {
        if (validator.isEmail(e.target.value.trim())) {
            this.setState({ email: { value: e.target.value.trim(), isValid: true, isModified : true} })
        } else {
            this.setState({ email: { value: e.target.value.trim(), isValid: false, isModified : true} })
        }
    }

    isValidPass = (e) => {
        if (e.target.id === "password") {
            if (!validator.isStrongPassword(e.target.value.trim(), {
                minLength: 8,
                minLowercase: 1,
                minUppercase: 1,
                minNumbers: 1,
                minSymbols: 1
            }) || e.target.value.includes(' ') ) {
                this.setState({ password: { value: e.target.value.trim(), isValid: false, isModified : true} })
            } else {
                this.setState({ password: { value: e.target.value.trim(), isValid: true, isModified : true} })
            }
        }
        if (e.target.id === "password-con") {
            if (e.target.value.trim() === this.state.password.value && this.state.password.isValid) {
                this.setState({ conPassword: { value: e.target.value.trim(), isValid: true, isModified : true} })
            } else {
                this.setState({ conPassword: { value: e.target.value.trim(), isValid: false, isModified : true} })
            }
        }
    }

    isChecked = (e) => {
        if (e.target.checked) {
            this.setState({ checked: {isValid:true, isModified : true} })
        } else {
            this.setState({ checked: {isValid: false, isModified : true} })
        }
    }

    select = (e) => {
        if (e.target.id === "gender") {
            if(e.target.value === "Select Gender") {
                this.setState({gender:{value:e.target.value, isValid:false, isModified : true} })
            } else {
                this.setState({gender:{value:e.target.value, isValid:true, isModified : true} })
            }
        }
        if (e.target.id === "role") {
            if(e.target.value === "Select Role") {
                this.setState({role:{value:e.target.value, isValid:false, isModified : true} })
            } else {
                this.setState({role:{value:e.target.value, isValid:true, isModified : true} })
            }
        }
    }

    isValidForm = (e) => {
        const isItValid =
            this.state.firstName.isValid &&
            this.state.lastName.isValid &&
            this.state.age.isValid &&
            this.state.gender.isValid &&
            this.state.role.isValid &&
            this.state.email.isValid &&
            this.state.password.isValid &&
            this.state.conPassword.isValid &&
            this.state.checked.isValid

        if (!this.state.firstName.isValid) {
            this.setState({firstName: { value: "", isValid: false, isModified:true }})
        }
        if (!this.state.lastName.isValid) {
            this.setState({lastName: { value: "", isValid: false, isModified:true }})
        }
        if (!this.state.age.isValid) {
            this.setState({age: { value: "", isValid: false, isModified:true }})
        }
        if (!this.state.gender.isValid) {
            this.setState({gender: { value: "", isValid: false, isModified:true }})
        }
        if (!this.state.role.isValid) {
            this.setState({role: { value: "", isValid: false, isModified:true }})
        }
        if (!this.state.email.isValid) {
            this.setState({email: { value: "", isValid: false, isModified:true }})
        }
        if (!this.state.password.isValid) {
            this.setState({password: { value: "", isValid: false, isModified:true }})
        }
        if (!this.state.conPassword.isValid) {
            this.setState({conPassword: { value: "", isValid: false, isModified:true }})
        }
        if (!this.state.checked.isValid) {
            this.setState({checked: { value: "", isValid: false, isModified:true }})
        }
        

        if (isItValid) {
            this.setState({ isValidForm: true }, () => {
                console.log(this.state)
            })
        }
    }

    render() {
        return (
            this.state.isValidForm ?
                <h1 className="h1 text-center mt-5 text-white">The form has been submitted successfully</h1>
                :
                <div className="container mt-5 bg-white rounded">
                    <div className="text-center">
                        <h2 className='pt-3'>Sign Up</h2>
                    </div>
                    <div className="row justify-content-center my-5">
                        <form
                            className='row needs-validation'
                            noValidate
                            onSubmit={(e) => {
                                e.preventDefault()
                                this.isValidForm(e)
                            }}
                        >
                            <Input
                                for="first"
                                type="text"
                                desc="First Name"
                                lable="First Name"
                                icon="icons/user.svg"
                                submit={this.isValidName}
                                validation={
                                    this.state.firstName.isModified ? 
                                    this.state.firstName.isValid?
                                    "is-valid"
                                    :
                                    "is-invalid"
                                    : ""
                                }
                                message="First name cannot be empty and can't contain a number"
                            />
                            <Input
                                for="last"
                                type="text"
                                desc="Last Name"
                                lable="Last Name"
                                icon="icons/user.svg"
                                submit={this.isValidName}
                                validation={
                                    this.state.lastName.isModified ? 
                                    this.state.lastName.isValid?
                                    "is-valid"
                                    :
                                    "is-invalid"
                                    : ""
                                }
                                message="Last name cannot be empty and can't contain a number"
                            />
                            <Input
                                for="age"
                                type="number"
                                desc="Age"
                                lable="Age"
                                icon="icons/age.svg"
                                submit={this.isValidAge}
                                validation={
                                    this.state.age.isModified ? 
                                    this.state.age.isValid?
                                    "is-valid"
                                    :
                                    "is-invalid"
                                    : ""
                                }
                                message="Age should be between 1-150 and cannot be a float"
                            />
                            <Select
                                lable="Gender"
                                values={["Select Gender","Male", "Female", "Other"]}
                                for="gender"
                                select={this.select}
                                validation={
                                    this.state.gender.isModified ? 
                                    this.state.gender.isValid?
                                    "is-valid"
                                    :
                                    "is-invalid"
                                    : ""
                                }
                                icon="icons/gender.svg"
                                message='Please select your gender'
                            />
                            <Select
                                lable="Role"
                                values={["Select Role","Developer", "Senior Developer", "Lead Engineer", "CTO"]}
                                for="role"
                                select={this.select}
                                validation={
                                    this.state.role.isModified ? 
                                    this.state.role.isValid?
                                    "is-valid"
                                    :
                                    "is-invalid"
                                    : ""
                                }
                                icon="icons/role.svg"
                                message='Please select your role'
                            />
                            <Input
                                for="email"
                                type="email"
                                desc="Email"
                                lable="Email address"
                                icon="icons/email.svg"
                                submit={this.isValidEmail}
                                validation={
                                    this.state.email.isModified ? 
                                    this.state.email.isValid?
                                    "is-valid"
                                    :
                                    "is-invalid"
                                    : ""
                                }
                                message="Not a valid email"
                            />
                            <Input
                                for="password"
                                type="password"
                                desc="Password"
                                lable="Password"
                                icon="icons/pass.svg"
                                submit={this.isValidPass}
                                validation={
                                    this.state.password.isModified ? 
                                    this.state.password.isValid?
                                    "is-valid"
                                    :
                                    "is-invalid"
                                    : ""
                                }
                                message={
                                    <ul>
                                        {this.state.password.value.length >= 8 ? <li className='text-success'>Atleast 8 characters long</li> : <li className='text-danger'>Atleast 8 characters long</li>}
                                        {this.state.password.value !== this.state.password.value.toUpperCase() ? <li className='text-success'>Contains a lowercase letter</li> : <li className='text-danger'>Contains a lowercase letter</li>}
                                        {this.state.password.value !== this.state.password.value.toLowerCase() ? <li className='text-success'>Contains an uppercase letter</li> : <li className='text-danger'>Contains an uppercase letter</li>}
                                        {this.state.password.value.split('').find((character) => {
                                            return !isNaN(parseInt(character))
                                        }) ? <li className='text-success'>Contains a number</li> : <li className='text-danger'>Contains a number</li>}
                                        {this.state.password.value.split('').find((character)=> {
                                            return "!@#$%^&*()_-+=|{}[]\\;':\"<>,.?/~`".includes(character)
                                        }) ?  <li className='text-success'>Contains a symbol</li> : <li className='text-danger'>Contains a symbol</li> }
                                    </ul>
                                }
                            />
                            <Input
                                for="password-con"
                                type="password"
                                desc="Confirm Password"
                                lable="Confirm Password"
                                icon="icons/pass.svg"
                                submit={this.isValidPass}
                                validation={
                                    this.state.conPassword.isModified ? 
                                    this.state.conPassword.isValid?
                                    "is-valid"
                                    :
                                    "is-invalid"
                                    : ""
                                }
                                message={this.state.password.isValid ? "Password does not match" : "Enter the valid password first"}
                            />
                            <Check
                                check={this.isChecked}
                                validation={
                                    this.state.checked.isModified ? 
                                    this.state.checked.isValid?
                                    "is-valid"
                                    :
                                    "is-invalid"
                                    : ""
                                }
                            >
                            I agree to terms and conditions.
                            </Check>
                            <Btn>Submit</Btn>
                        </form>
                    </div>
                </div>
        )
    }
}

export default Form