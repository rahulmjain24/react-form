import React from 'react'

class Check extends React.Component {
    render() {
        return (
            <div className="form-check has-validation input-group">
                <input 
                    onClick={(e) => {
                        this.props.check(e)
                    }} 
                    className={`form-check-input ms-0 me-3 ${this.props.validation}`}
                    type="checkbox" 
                    value="" 
                    id="flexCheckDefault" 
                    required
                />
                <label className="form-check-label" htmlFor="flexCheckDefault">
                    {this.props.children}
                </label>
                <div className="invalid-feedback">
                    Tos needs to be accepted
                </div>
            </div>
        )
    }
}

export default Check