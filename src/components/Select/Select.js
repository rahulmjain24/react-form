import React from 'react'

class Select extends React.Component {
    render() {
        return (
            <div className="mb-3">
                <label htmlFor={this.props.for} className="form-label">{this.props.lable}</label>
                <div className="input-group">
                <span className="input-group-text">
                        <img 
                            src={this.props.icon} 
                            alt="icon" 
                        />
                    </span>
                    <select id={this.props.for}
                        onChange={(e) => {
                            this.props.select(e)
                        }}
                        className={`form-select ${this.props.validation}`} aria-label={this.props.default} required>
                        {this.props.values.map((value) => {
                            return <option key={value} value={value}>{value}</option>
                        })}
                    </select>
                    <div className="invalid-feedback">
                        {this.props.message}
                    </div>
                </div>
            </div>
        )
    }
}

export default Select