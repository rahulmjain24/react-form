import React from 'react'

class Btn extends React.Component {
    render() {
        return (
            <div className="mb-4 mt-3 text-center">
                <button type="submit" className="btn btn-primary">{this.props.children}</button>
            </div>
        )
    }
}

export default Btn